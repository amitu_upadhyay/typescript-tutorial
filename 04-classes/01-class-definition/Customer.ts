class Customer {

    firstName: string;
    lastName: string;

    constructor(firstName: string, lastName: string){
        this.firstName = firstName;
        this.lastName = lastName;
    }

}

// lets create an instance 
let myCustomer = new Customer("Amit", "Upadhyay");

//let firstName = myCustomer.firstName = "Mark";
//let lastName = myCustomer.lastName = "Henery";

//console.log(`Customer Name is ${firstName} ${lastName} `);

console.log(`Customer Name is ${myCustomer.firstName} ${myCustomer.lastName} `);