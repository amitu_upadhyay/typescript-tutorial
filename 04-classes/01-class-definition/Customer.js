var Customer = /** @class */ (function () {
    function Customer(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return Customer;
}());
// lets create an instance 
var myCustomer = new Customer("Amit", "Upadhyay");
//let firstName = myCustomer.firstName = "Mark";
//let lastName = myCustomer.lastName = "Henery";
//console.log(`Customer Name is ${firstName} ${lastName} `);
console.log("Customer Name is " + myCustomer.firstName + " " + myCustomer.lastName + " ");
