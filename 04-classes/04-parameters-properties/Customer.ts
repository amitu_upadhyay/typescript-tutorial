class Customer {

    constructor(private _firstName:string, private _lastName:string){

    }

    public get firstName(){
        return this._firstName;
    }

    public set firstName(firstName: string){
            this._firstName = firstName;
    }

    public get lastName(): string {
        return this._lastName;
    }
    public set lastName(value: string) {
        this._lastName = value;
    }

}

// lets create an instance 
let myCustomer = new Customer("Amit", "Upadhyay");

//let firstName = myCustomer.firstName = "Mark";
//let lastName = myCustomer.lastName = "Henery";

//console.log(`Customer Name is ${firstName} ${lastName} `);

console.log(`Customer Name is ${myCustomer.firstName} ${myCustomer.lastName} `);