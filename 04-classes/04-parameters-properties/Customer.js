"use strict";
var Customer = /** @class */ (function () {
    function Customer(_firstName, _lastName) {
        this._firstName = _firstName;
        this._lastName = _lastName;
    }
    Object.defineProperty(Customer.prototype, "firstName", {
        get: function () {
            return this._firstName;
        },
        set: function (firstName) {
            this._firstName = firstName;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Customer.prototype, "lastName", {
        get: function () {
            return this._lastName;
        },
        set: function (value) {
            this._lastName = value;
        },
        enumerable: false,
        configurable: true
    });
    return Customer;
}());
// lets create an instance 
var myCustomer = new Customer("Amit", "Upadhyay");
//let firstName = myCustomer.firstName = "Mark";
//let lastName = myCustomer.lastName = "Henery";
//console.log(`Customer Name is ${firstName} ${lastName} `);
console.log("Customer Name is " + myCustomer.firstName + " " + myCustomer.lastName + " ");
