import { Shape } from "./Shape"; 
import { Circle } from "./Circle";
import { Rectangle } from  "./Rectangle";

//let myShape = new Shape(3,4);

//console.log("Shape => "+myShape.getInfo());

let myCircle = new Circle(1,3,5);

console.log("Circle => "+ myCircle.getInfo());

let myRectangle = new Rectangle(2,5,4,6);

console.log("Rectangle => "+myRectangle.getInfo());


