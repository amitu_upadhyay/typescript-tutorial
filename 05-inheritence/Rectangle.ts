import { Shape } from "./Shape"; 

export class Rectangle extends Shape{
    
    
    constructor(theX: number, theY:number,  private _hight: number, private _width: number){
        
        super(theX, theY);
    }

    public get hight(): number {
        return this._hight;
    }
    public set hight(value: number) {
        this._hight = value;
    }

    public get width(): number {
        return this._width;
    }
    public set width(value: number) {
        this._width = value;
    }

    public getInfo(): string{

        return super.getInfo() + `height = ${this.hight}, width = ${this.width}`

    }

    calculateArea(): number {
        return this._hight * this._width ;
    }


}