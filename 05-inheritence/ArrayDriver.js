"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Circle_1 = require("./Circle");
var Rectangle_1 = require("./Rectangle");
//let myShape = new Shape(3,4);
var myCircle = new Circle_1.Circle(1, 3, 5);
var myRectangle = new Rectangle_1.Rectangle(2, 5, 4, 6);
// Declare an Empty Array of Shape
var theShapes = [];
//theShapes.push(myShape);
theShapes.push(myCircle);
theShapes.push(myRectangle);
console.log(theShapes);
for (var _i = 0, theShapes_1 = theShapes; _i < theShapes_1.length; _i++) {
    var tempShape = theShapes_1[_i];
    console.log(tempShape.getInfo());
    console.log("Area => " + tempShape.calculateArea());
    console.log();
}
