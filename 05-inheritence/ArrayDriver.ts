import { Shape } from "./Shape"; 
import { Circle } from "./Circle";
import { Rectangle } from  "./Rectangle";

//let myShape = new Shape(3,4);
let myCircle = new Circle(1,3,5);
let myRectangle = new Rectangle(2,5,4,6);

// Declare an Empty Array of Shape

let theShapes: Shape[] = [];

//theShapes.push(myShape);
theShapes.push(myCircle);
theShapes.push(myRectangle);

console.log(theShapes);

for(let tempShape of theShapes){
    console.log(tempShape.getInfo());
    console.log("Area => "+tempShape.calculateArea());
    console.log();
}



