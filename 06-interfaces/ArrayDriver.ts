import { CricketCoach } from "./CricketCoach";
import { Coach } from "./Coach";
import { RunningCoach } from "./RunningCoach";

let theCoaches: Coach[] = [];

let myCricketCoach = new CricketCoach();
let myRunningCoach = new RunningCoach();

theCoaches.push(myRunningCoach);

theCoaches.push(myCricketCoach);

for(let tempCoaches of theCoaches){
    console.log(tempCoaches.getDailyWorkout());
}