import { CricketCoach } from "./CricketCoach";
import { Coach } from "./Coach";
import { RunningCoach } from "./RunningCoach";


let myCricketCoach = new CricketCoach();
console.log(myCricketCoach.getDailyWorkout());

let myRunningCoach = new RunningCoach();
console.log(myRunningCoach.getDailyWorkout());
