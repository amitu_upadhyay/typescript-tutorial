"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RunningCoach = void 0;
var RunningCoach = /** @class */ (function () {
    function RunningCoach() {
    }
    RunningCoach.prototype.getDailyWorkout = function () {
        return "Practice Running 5km Daily.";
    };
    return RunningCoach;
}());
exports.RunningCoach = RunningCoach;
