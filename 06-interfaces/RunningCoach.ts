import { Coach } from "./Coach";


export class RunningCoach implements Coach{
    
    getDailyWorkout(): string {
       return "Practice Running 5km Daily.";
    }
    
}